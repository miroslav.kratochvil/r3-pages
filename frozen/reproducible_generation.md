---
layout: default
order: -1
title: Reproducible generation of human midbrain organoids for in vitro modeling of Parkinson's disease
permalink: /frozen/reproducible-generation-of-human-midbrain-organoids-for-in-vitro-modeling-of-parkinsons-disease
---

{% rtitle Reproducible generation of human midbrain organoids for in vitro modeling of Parkinson`s disease %}
Sarah Louise Nickels†, Jennifer Modamio, Bárbara Mendes-Pinheiro, Anna Sophia Monzel, Fay Betsou and Jens Christian Schwamborn*
{% endrtitle %} 


{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-video %}
The complete Dataset is available  [here](https://webdav-r3lab.uni.lu/public/data/reproducible-generation-of-human-midbrain-organoids-for-in-vitro-modeling-of-parkinsons-disease/). It is subdivided into originals (raw data) and partials (analysis) specific to each figure and supplementary present in the manuscript.
{% endrblock %}


{% rblock source code %}
The source code used to make the analysis/figures of the publication is available on [Github](https://github.com/LCSB-DVB/Nickels_2019).
{% endrblock %}

{% endrgridblock %} 
