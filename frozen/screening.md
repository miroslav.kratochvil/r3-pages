---
layout: default
order: -1
title: Using High-Content Screening to Generate Single-Cell Gene-Corrected Patient-Derived iPS Clones Reveals Excess Alpha-Synuclein with Familial Parkinson’s Disease Point Mutation A30P
permalink: /frozen/screening
---

{% rtitle Using High-Content Screening to Generate Single-Cell Gene-Corrected Patient-Derived iPS Clones Reveals Excess Alpha-Synuclein with Familial Parkinson’s Disease Point Mutation A30P %}

Peter A. Barbuti, Paul M. Antony, Bruno FR. Santos, François Massart, Gérald Cruciani, Claire M. Dording, Jonathan Arias, Jens C. Schwamborn, Rejko Krüger
<center>
<img src="{{ "images/screening.png" | relative_url }}" width="90%" />
</center>
{% endrtitle %}

{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-video %}
The complete dataset is available [here](https://webdav-r3lab.uni.lu/public/data/screening/).
{% endrblock %}

{% rblock source code %}
The source code used to make the analysis/figures of the publication is available on [Gitlab](https://gitlab.lcsb.uni.lu/LCSB-CEN/cloneclassifier).
{% endrblock %}

{% endrgridblock %}