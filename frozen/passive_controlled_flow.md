---
layout: default
order: -1
title:  Passive controlled flow for neuronal cell culture in 3D microfluidic devices
permalink: /frozen/passive-controlled-flow-for-neuronal-cell-culture-in-3d-microfluidic-devices
---

{% rtitle  Passive controlled flow for neuronal cell culture in 3D microfluidic devices %}
 Please cite the article on <a href="#">Paper</a>.<br/>
Khalid I.W. Kane, Javier Jarazo, Edinson Lucumi Moreno, Paul Vulto, Ronan M.T. Fleming , Bas Trietsch, Jens C. Schwamborn*
{% endrtitle %} 


{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-image %}
The complete **Dataset** is available [here](https://webdav-r3lab.uni.lu/public/data/passive-controlled-flow-for-neuronal-cell-culture-in-3d-microfluidic-devices/). It is subdivided into originals (raw data) and partials (analysis) specific to each figure and supplementary present in the manuscript.
{% endrblock %}


{% endrgridblock %} 
