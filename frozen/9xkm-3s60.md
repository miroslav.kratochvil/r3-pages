---
layout: default
order: -1
title: Dibac
permalink: /frozen/9xkm-3s60
---


{% rtitle Dibac: Distribution-Based Analysis Of Cell Differentiation Identifies Mechanisms Of Cell Fate %}

The recent developments in single cell genomics allow for in-depth characterization of cellular heterogeneity in tissue development and the identification of new regulatory mechanisms. Despite these achievements, our understanding of underlying principles in cell fate dynamics is still rather limited.  Here, we present a new approach that exploits the high dimensional transcription distributions of single cell RNA sequencing (sc-RNAseq) data by information theory-based measures, which allow for robust identification of cell differentiation properties and efficient differentially expressed gene (DEG) analysis. We show that appropriate binarization of single cell transcription data allows for the rigorous definition of mutual information and robust entropy measures that reflect the general properties of cell fate decisions. We exemplify our distribution-based analysis of cell differentiation (DiBAC) with single cell qPCR data of blood cell development and sc-RNAseq data of Parkinson's disease-related iPSC differentiation into dopaminergic neurons.

permalink: [doi:10.17881/9xkm-3s60](https://doi.org/10.17881/9xkm-3s60)

{% endrtitle %}


{% rblock Code %}
The source code cand be found on [Giltab](https://gitlab.lcsb.uni.lu/susan.ghaderi/dibac/)
{% endrblock %}

{% rblock Data %}
The Supplementary Information can also be found [here](https://webdav-r3lab.uni.lu/public/data/9xkm-3s60/).
{% endrblock %}
