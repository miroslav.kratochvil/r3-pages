---
layout: default
order: -1
title: Midbrain organoids mimic early embryonic neurodevelopment and recapitulate LRRK2-G2019S - associated gene expression
permalink: /frozen/rc4f-nk07
---


{% rtitle Midbrain organoids mimic early embryonic neurodevelopment and recapitulate LRRK2-G2019S - associated gene expression %}
Alise Zagare, Kyriaki Barmpa, Semra Smajic, Lisa Smits, Kamil Grzyb, Anne Grünewald, Alexander Skupin, Sarah Louise Nickels and Jens Christian Schwamborn
{% endrtitle %}

{% rblock Abstract %}
Human brain organoid models that recapitulate the physiology and complexity of the human brain have a great potential for in vitro disease modeling, in particular for neurodegenerative diseases, such as Parkinson’s disease. In the present study, we compare single-cell RNA sequencing data of human midbrain organoids to the developing human embryonic midbrain. We demonstrate that the in vitro model is comparable to its in vivo equivalents in terms of developmental path and cellular composition. Moreover, we investigate the potential of midbrain organoids for modeling early developmental changes in Parkinson’s disease. Therefore, we compare the single cell-RNA sequencing data of healthy individual-derived midbrain organoids to their isogenic LRRK2-G2019S inserted/mutated counterparts. We show that the LRRK2-G2019S mutation alters neurodevelopment, resulting in an untimely and incomplete differentiation with reduced cellular variability. Finally, we present four candidate genes APP, DNAJC6, GATA3 and PTN that might contribute to the LRRK2-G2019S associated transcriptome changes during early neurodevelopment.
{% endrblock %}

{% rgridblock a-unique-id %}

{% rblock  Raw data | fas fa-database %}
The complete Dataset is available [here](https://webdav-r3lab.uni.lu/public/data/rc4f-nk07/). It is subdivided into originals (raw data) and partials (analysis) specific to each figure and supplementary figure present in the manuscript.
{% endrblock %}

{% rblock  RNA-seq data | fas fa-database %}
The different RNA-seq datasets used in the study are available here: [Embryo Cortex - GSE104276](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE104276); [Embryo Midbrain - GSE76381](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE76381) and [Cortex Organoid - GSE130238](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE130238).
{% endrblock %}

{% rblock Scripts %}
The source code used to make the analysis/figures of the publication is available [on Github](https://github.com/LCSB-DVB/Zagare_Barmpa_2021).
{% endrblock %}

{% endrgridblock %}