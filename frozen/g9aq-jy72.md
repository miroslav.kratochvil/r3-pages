---
layout: default
order: -1
title: Identification of tissue-specific and common methylation quantitative trait loci in healthy individuals using MAGAR
permalink: /frozen/g9aq-jy72
---


{% rtitle Identification of tissue-specific and common methylation quantitative trait loci in healthy individuals using MAGAR %}

[https://syscid.eu](https://syscid.eu)

Understanding the influence of genetic variants on DNA methylation is fundamental for the interpretation of epigenomic data in the context of disease. The dataset developed in this project provides matched genotyping and DNA methylation data in four tissues (ileum, rectum, T-cells, B-cells) from healthy individuals. Using this data, we demonstrate the discrimination of common from cell-type-specific methQTLs. Our analysis demonstrates that a systematic analysis of methQTLs provides important new insights on the influences of genetic variants to cell-type-specific epigenomic variation.

permalink: [doi:10.17881/g9aq-jy72](https://doi.org/10.17881/g9aq-jy72)
{% endrtitle %}


{% rblock Data access %}
Data is available [here](https://owncloud.lcsb.uni.lu/s/cSlkNk1doxIzORW). The data sustainability is provided by ELIXIR Luxembourg via its [data hosting services](https://elixir-luxembourg.org/services).

<br><br>
<div align="center">
<img src="{{ "images/elixir-logo.svg" | relative_url }}" width="20%" />
</div>
{% endrblock %}

{% rblock Publications | fas fa-scroll %}
Identification of tissue-specific and common methylation quantitative trait loci in healthy individuals using MAGAR (submitted).
{% endrblock %}
