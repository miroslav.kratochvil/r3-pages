---
layout: default
order: -1
title: Molecular reclassification to find clinically useful biomarkers for systemic autoimmune diseases
permalink: /frozen/th9v-xt85
---


{% rtitle PRECISESADS: Molecular reclassification to find clinically useful biomarkers for systemic autoimmune diseases %}
[https://www.imi.europa.eu/projects-results/project-factsheets/precisesads](https://www.imi.europa.eu/projects-results/project-factsheets/precisesads)

PRECISESADS cohort includes whole blood molecular information for seven systemic autoimmune disorders (Systemic Lupus Erythematosus, Rheumatoid Arthritis, Sjogrën’s Syndrome, Systemic Sclerosis, Mixed Connective Tissue Disease, Primary Antiphospholipid Syndrome and Undifferentiated Connective Tissue Disease) and healthy controls. A cross-sectional study and an inception study prospectively followed for 6 and 14 months are included in the dataset. The molecular information comprise transcriptome, methylome, flow-cytometry and other serological information as cytokines and autoantibodies.

permalink: [doi:10.17881/th9v-xt85](https://doi.org/10.17881/th9v-xt85)
{% endrtitle %}


{% rblock Data access %}
PRECISESADS data is hosted by [ELIXIR Luxembourg](https://elixir-luxembourg.org/services). The dataset is available research use under a controlled access model. To request access please contact the data stewardship team of ELIXIR Luxembourg via [lcsb-datastewards@uni.lu](mailto:lcsb-datastewards@uni.lu). 

<br><br>
<div align="center">
<img src="{{ "images/elixir-logo.svg" | relative_url }}" width="20%" />
</div>

{% endrblock %}

{% rblock Publications | fas fa-scroll %}

Integrative Analysis Reveals a Molecular Stratification of Systemic Autoimmune Diseases. Guillermo Barturen, Sepideh Babaei, Francesc Català-Moll et al. medRxiv 2020.02.21.20021618; doi: [https://doi.org/10.1002/art.41610](https://doi.org/10.1002/art.41610)

A new molecular classification to drive precision treatment strategies in primary Sjögren’s syndrome. Perrine Soret, Christelle Le Dantec, Emiko Desvaux et al. Nat Commun 12, 3523 (2021). doi: [https://doi.org/10.1038/s41467-021-23472-7](https://doi.org/10.1038/s41467-021-23472-7)

{% endrblock %}

