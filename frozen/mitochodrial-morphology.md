---
layout: default
order: -1
title: Mitochondrial morphology provides a mechanism for energy buffering at synapses
permalink: /frozen/mitochondrial-morphology-provides-a-mechanism-for-energy-buffering-at-synapses
---


{% rtitle Mitochondrial morphology provides a mechanism for energy buffering at synapses %}
Please cite the article on <a href="https://doi.org/10.1038/s41598-019-54159-1">Nature Scientific Reports</a>.<br/>
Guadalupe C. Garcia†, Thomas M. Bartol, Sebastien Phan, Eric A. Bushong, Guy Perkins, Terrence J. Sejnowski, Mark H. Ellisman, Alexander Skupin*.
{% endrtitle %}



{% rgridblock a-unique-id %}


{%  rblock Movies | fas fa-video %}
The Movies can be found <a href="https://webdav-r3lab.uni.lu/public/data/mitomorpho/movies">here</a>.
{% endrblock %}

{%  rblock Dataset | fas fa-th %}
The dataset is available <a href="https://webdav-r3lab.uni.lu/public/data/mitomorpho/dataset">here</a>.
{% endrblock %}


{%  rblock Source code | fas fa-code %}
The source code is available on the <a href="https://gitlab.lcsb.uni.lu/guadalupe.garcia/mitochondria-morphology">LCSB Gitlab</a> where you can traceback what have been done by the authors.
{% endrblock %}


{% endrgridblock %}

