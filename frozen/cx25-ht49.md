---
layout: default
order: -1
title: Microglia integration into human midbrain organoids shows increased neuronal maturation and functionality
permalink: /frozen/cx25-ht49
---

{% rtitle Microglia integration into human midbrain organoids shows increased neuronal maturation and functionality %}
Sonia Sabate-Soler, Sarah Louise Nickels, Cláudia Saraiva, Emanuel Berger, Ugnė Dubonyte, Yan Jun Lan, Tsukasa Kouno, , Javier Jarazo, Graham Robertson, Jafar Sharif, Haruhiko Koseki, Christian Thome, Jay W. Shin, Sally Cowley, Jens C. Schwamborn
{% endrtitle %} 


{% rgridblock a-unique-id %}
{% rblock Figures | fas fa-image %}
The data for the figures can be found [here](https://webdav-r3lab.uni.lu/public/data/cx25-ht49/).
{% endrblock %}

{% rblock Code | fas fa-th %}
The source code used to produce the figures can be found [here](https://github.com/LCSB-DVB/Sabate-Soler-2021).
{% endrblock %}


{% endrgridblock %} 
