---
layout: default
order: -1
title: "Retrograde procedural memory in Parkinson's disease: a case-control study"
permalink: /frozen/7bwb-aj16
---

{% rtitle Retrograde procedural memory in Parkinson's disease: a case-control study %}
Laure PAULY, Claire PAULY, Maxime HANSEN, Valerie E. SCHRÖDER, Armin RAUSCHENBERGER, Anja K. LEIST & Rejko KRÜGER on behalf of the NCER-PD Consortium
{% endrtitle %}

{% rgridblock %}

{% rblock Data | fas fa-database %}

The datasets for this manuscript are not publicly available as they are linked to the Luxembourg Parkinson’s Study and its internal regulations. Requests to access the datasets should be directed to the Executive Committee of NCER-PD, mean of contact via email: <a href="mailto:request.ncer-pd@uni.lu">request.ncer-pd@uni.lu</a>.

{% endrblock %}

{% rblock source code %}

The source code is available <a href="https://gitlab.lcsb.uni.lu/TNG/papers/RPMemory">here</a>.

{% endrblock %}

{% endrgridblock %}