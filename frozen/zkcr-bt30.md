---
layout: default
order: -1
title: "COBREXA.jl: constraint-based reconstruction andexascale analysis"
permalink: /frozen/zkcr-bt30
---

{% rtitle COBREXA.jl: constraint-based reconstruction andexascale analysis %}

Miroslav Kratochvíl, Laurent Heirendt, St. Elmo Wilken, TaneliPusa, Sylvain Arreckx, Alberto Noronha, Marvin van Aalst, Venkata P. Satagopam, Oliver Ebenhöh, Reinhard Schneider, ChristopheTrefois, and Wei Gu

{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock source code | fas fa-code-branch %}
**COBREXA.jl** source code is hosted on [Github](https://github.com/LCSB-BioCore/COBREXA.jl).
{% endrblock %}

{% rblock documentation | fas fa-book %}
The documentation of **COBREXA.jl** is available [here](https://lcsb-biocore.github.io/COBREXA.jl).
{% endrblock %}

{% rblock benchmarks | fas fa-code %}
The benchmark scripts are available [here](https://gitlab.lcsb.uni.lu/R3/outreach/papers/cobrexa/benchmarks).
{% endrblock %}

{% endrgridblock %}
