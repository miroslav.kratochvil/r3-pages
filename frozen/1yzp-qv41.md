---
layout: default
order: -1
title: "Synaptic decline precedes dopaminergic neuronal loss in human midbrain organoids harboring a triplication of the SNCA gene"
permalink: /frozen/1yzp-qv41
---

{% rtitle Synaptic decline precedes dopaminergic neuronal loss in human midbrain organoids harboring a triplication of the SNCA gene %}
Jennifer Modamio, Cláudia Saraiva, Gemma Gomez-Giro, Sarah Nickels, Javier Jarazo, Paul Antony, Silvia Bolognin, Peter Barbuti, Rashi Halder, Christian Jäger, Rejko Krueger, Enrico Glaab, Jens Schwamborn
{% endrtitle %}

{% rgridblock a-unique-id %}
{% rblock  Manuscript %}
A preprint of the manuscript is available <a href="">here</a>.
{% endrblock %}


{% rblock Data %}
The data is available <a href="https://webdav-r3lab.uni.lu/public/data/1yzp-qv41/">here</a>.
{% endrblock %}


{% rblock source code %}
The source code is available <a href="https://github.com/LCSB-DVB/Modamio_2021">here</a>.
{% endrblock %}

{% endrgridblock %}


