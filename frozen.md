---
layout: default
title: Frozen pages
order: 2
permalink: /frozen/
---

# Information
Here you can find project pages that are related to a published scientific paper. Project pages must link to all components referenced in the paper and is not modified after the release of the paper.

This website is under version control on the [LCSB Gitlab](https://gitlab.lcsb.uni.lu/core-services/r3-pages).

# Frozen pages

* **[Modeling Parkinson's disease in midbrain like organoids]({{ "frozen/modeling-parkinsons-disease-in-midbrain-like-organoids" | relative_url }})**

* **[Synapse Alterations Precede Neuronal Damage And Storage Pathology In A Human Cerebral Organoid Model]({{ "frozen/synapse-alterations-precede-neuronal-damage-and-storage-pathology-in-a-human-cerebral-organoid-model" | relative_url }})**

* **[IMP - Integrated Metaomic Pipeline]({{ "frozen/imp" | relative_url }})**

* **[Reproducible generation of human midbrain organoids for in vitro modeling of Parkinson's disease]({{ "frozen/reproducible-generation-of-human-midbrain-organoids-for-in-vitro-modeling-of-parkinsons-disease" | relative_url }})**

* **[SmartR]({{ "frozen/smartr" | relative_url }})** - an open-source platform for interactive visual analytics for translational research data

* **[Single-cell transcriptomics reveals multiple neuronal cell types in human midbrain-specific organoids]({{ "frozen/single-cell-transcriptomics-reveals-multiple-neuronal-cell-types-in-human-midbrain-specific-organoids" | relative_url }})**

* **[Machine learning-assisted neurotoxicity prediction in human midbrain organoids]({{ "frozen/machine-learning-assisted-neurotoxicity-prediction-in-human-midbrain-organoids" | relative_url }})**

* **[Passive controlled flow for neuronal cell culture in 3D microfluidic devices]({{ "frozen/passive-controlled-flow-for-neuronal-cell-culture-in-3d-microfluidic-devices" | relative_url }})**

* **[Using high-content screening technology as a tool to generate single-cell patient-derived gene-corrected isogenic iPS clones for Parkinson’s disease research]({{ "frozen/screening" | relative_url }})**

* **[Clustering and visualizing huge-scale cytometry datasets with GigaSOM.jl]({{ "frozen/gigasom" | relative_url }})**

* **[Mitochondrial morphology provides a mechanism for energy buffering at synapses]({{ "frozen/mitochondrial-morphology-provides-a-mechanism-for-energy-buffering-at-synapses" | relative_url }})**

* **[Retrograde procedural memory in parkinson's disease: a case-control study]({{ "frozen/7bwb-aj16" | relative_url }})**

* **[Retrospective Non-target Analysis to Support Regulatory Water Monitoring: From Masses of Interest to Recommendations via in silico workflows]({{ "frozen/40ss-ft75" | relative_url }})**

* **[Identification of tissue-specific and common methylation quantitative trait loci in healthy individuals using MAGAR]({{ "frozen/g9aq-jy72" | relative_url }})**

* **[COBREXA.jl: constraint-based reconstruction andexascale analysis]({{ "frozen/zkcr-bt30" | relative_url }})**

* **[Midbrain organoids mimic early embryonic neurodevelopment and recapitulate LRRK2-G2019S - associated gene expression]({{ "frozen/rc4f-nk07" | relative_url }})**

* **[Functional meta-omics provide critical insights into long and short read assemblies]({{ "frozen/sgzt-ad12" | relative_url }})**

* **[Protein Relative Abundance Quantification Algorithm for 3d fluorescent images from tissue]({{ "frozen/PRAQA" | relative_url }})**

* **[Synaptic decline precedes dopaminergic neuronal loss in human midbrain organoids harboring a triplication of the SNCA gene]({{ "frozen/1yzp-qv41" | relative_url }})**

* **[ A "two-hit" pharmacological seizure model in zebrafish for studying microglia dynamics in the developing epileptic brain]({{ "frozen/77zg-bc41" | relative_url }})**

* **[Method optimization of skin biopsy-derived fibroblast culture for reprogramming into induced pluripotent stem cells (iPSCs)]({{ "frozen/255d-4a98" | relative_url }})**
